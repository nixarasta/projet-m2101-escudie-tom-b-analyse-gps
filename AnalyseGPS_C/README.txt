MINI PROJET M2101 

ESCUDIE TOM (groupe B)

Ce petit programme prend en entrée des trames NMEA (GPGGA) et en sortie écrit sur un fichier texte les données
converties et dans un format plus lisible.

/!\ Le programme compile sur CODE BLOCKS mais ne compile pas avec le makefile (comparaison avec un pointeur... cette
erreur passe sur l'IDE, je n'ai pas le temps de tout corriger. /!\

Le résultat fourni par l'exécution du programme avec l'IDE se trouve dans le fichier "resultats_programmes.txt"

Documentation fonctions :

int getIndiceChamp (char* trame, int numChamp);
=> entrée : trame GPGGA, entier | sortie : position du champ numéro numChamp

int getLongueurChamp (char* trame, int numChamp);
=> entrée : trame GPGGA, entier | sortie : longueur du champ numéro numChamp

int verifierTrame(char* trame);
=> entrée : trame GPGGA  | sortie : 1 si la trame respecte quelques critères (longeurs de champs, nombre de champs...) sinon 0

char* extraireChamps(char* trame, int numChamp);
=> entrée : trame GPGGA  | sortie : champ numéro numChamp

char* extraireSecondesDArc(char* champLatitude);
=> entrée : champLatitude (ou longitude)  | sortie : Le champ sous forme de chaîne de caractères

char* convertirHeure(char* champHeure);
=> entrée : Champ heure (152536)  | sortie : Champ heure formaté (15h25m36s)

char* convertirLatitude(char* champLatitude, char* champLatitudeCardinal);
=> entrée : Champ latitude xxxx.yyyy | sortie : xx*xx'yy" avec yy converti en base 60 (*100 /60)

char* convertirLongitude(char* champLongitude, char* champLongitudeCardinal);
=> entrée : Champ longitude xxxxx.yyyy | sortie : xxx*xx'yy" avec yy converti en base 60 (*100 /60)
