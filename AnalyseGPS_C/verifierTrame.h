/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verifierTrame.c                                           *
*                                                                             *
******************************************************************************/

int verifierTrame(char* trame);
int getLongueurChamp (char* trame, int numChamp);
int getIndiceChamp (char* trame, int numChamp);
