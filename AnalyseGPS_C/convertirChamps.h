/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : convertirChamps.c                                         *
*                                                                             *
******************************************************************************/

char* convertirHeure(char* champHeure);
char* convertirLatitude(char* champLatitude, char* champLatitudeCardinal);
char* convertirLongitude(char* champLongitude, char* champLongitudeCardinal);
