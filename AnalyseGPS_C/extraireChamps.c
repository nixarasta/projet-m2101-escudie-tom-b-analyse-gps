/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : extraireChamps.c                                          *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "verifierTrame.h"
#include "extraireChamps.h"
#include "convertirChamps.h"

char* extraireChamps(char * trame, int numChamp) {

    char chaine[20] = "";
    for (int i = 0 ; i < getLongueurChamp(trame, numChamp) ; i++) {
        chaine[i] = trame[i+getIndiceChamp(trame, numChamp)+1];
    }

    char* copie=NULL;
    copie=malloc((getLongueurChamp(trame, numChamp))*sizeof(char));
    strcpy(copie,chaine);
    return copie;
}

char* extraireSecondesDArc(char * champLatitude)
{
    int i = 0;
    char chaine[20] = "";
    while (champLatitude[i] != NULL) {
        if (champLatitude[i] == 46) {
            int j = i+1;
            while (champLatitude[j] != NULL) {
                chaine[j-i-1] = champLatitude[j];
                j += 1;
            }
            char* copie=NULL;
            copie=malloc(13*sizeof(char));
            strcpy(copie,chaine);
            return copie;
        }
        i += 1;
    }
    return "aa";
}
