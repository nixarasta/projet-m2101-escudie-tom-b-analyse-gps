/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verifierTrame.c                                           *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "verifierTrame.h"
#include "extraireChamps.h"

int verifierTrame(char * trame) {
    int compteurVirgule = 0;
    int compteurEtoile = 0;
    int verificationGPGGA[7] = {36,71,80,71,71,65,44};
    int verificationTaille[5] = {10,9,1,10,1};

    for (int i = 0 ; i <= 6 ; i++) {
        if (trame[i] != verificationGPGGA[i]) {
            return 0;
        }
    }

    int i = 0;
    while (trame[i] != NULL) {
        if (trame[i] == 44) {
            compteurVirgule += 1;
        }
        if (trame[i] == 42) {
            compteurEtoile += 1;
        }
        i += 1;
    }
    if (compteurVirgule != 14 || compteurEtoile != 1) {
        return 0;
    }

    for (int i = 0 ; i <= 4 ; i++) {
        if (getLongueurChamp(trame, i+1) != verificationTaille[i]) {
            return 0;
        }
    }
    return 1;
}

int getLongueurChamp(char * trame,int numChamp) {
    int i = 0;
    int compteurVirgule = 0;
    int longueurChamp = 0;
    while (trame[i] != NULL) {
        if (trame[i] == 44) {
            compteurVirgule += 1;
        }
        int j = 0;

        if (compteurVirgule == numChamp) {
            j = i + 1;
            while (trame [j] != 44) {
                longueurChamp += 1;
                j += 1;
            }
            return longueurChamp;
        }
        i += 1;
    }
    return 0;
}

int getIndiceChamp (char* trame, int numChamp) {
    int i = 0;
    int compteurVirgule = 0;
    while (trame[i] != NULL) {
        if (trame[i] == 44) {
            compteurVirgule++;
            if (compteurVirgule == numChamp) {
                return i;
            }
        }
        i++;
    }
    return -1;
}
