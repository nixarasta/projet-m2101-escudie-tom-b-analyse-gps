/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "verifierTrame.h"
#include "extraireChamps.h"
#include "convertirChamps.h"

char* afficherInformations(char* trame);

void main()
{
    FILE* fic;
    fic = fopen("resultats_programme.txt", "w");
    if (fic == NULL) {
        printf("Erreur � l'ouverture du fichier texte\n");
    } else {
        fprintf(fic, "%s", "************************** Mini projet ANALYSE GPS ESCUDIE TOM S2B **************************\n\n");
        fprintf(fic, "%s", afficherInformations("$GPGGA,160000.288,4334.0209,N,00127.0896,E,1,12,1.0,0.0,M,0.0,M,,*61"));
        fprintf(fic, "%s", afficherInformations("$GPGGA,190003.288,4322.0123,N,00221.0991,E,1,12,1.0,0.0,M,0.0,M,,*6E"));
        fprintf(fic, "%s", afficherInformations("$GPGGA,165135.288,2351.0493,N,12055.0076,E,1,12,1.0,0.0,M,0.0,M,,*60"));
        fprintf(fic, "%s", afficherInformations("$GPGGA,102314.288,7709.0430,N,04242.0891,W,1,12,1.0,0.0,M,0.0,M,,*75"));
        fprintf(fic, "%s", afficherInformations("$GPGGA,225332.288,3727.0707,N,12626.0458,E,1,12,1.0,0.0,M,0.0,M,,*60"));
        fprintf(fic, "%s", afficherInformations("$GPGGA,225332.288,3727.0707,Ceci est un champ incorrect,12626.0458,E,1,12,1.0,0.0,M,0.0,M,,*60"));
        fprintf(fic, "%s", afficherInformations("$GPA,225332.288,3727.0707,N,12626.0458,E,1,12,1.0,0.0,M,0.0,M,,*60"));
        fprintf(fic, "%s", afficherInformations("$GPA,225332.288,champ supplementaire,3727.0707,N,12626.0458,E,1,12,1.0,0.0,M,0.0,M,,*60"));
    }
        fclose(fic);
}

char* afficherInformations(char* trame) {
    char chaine[200] = "";
    if (verifierTrame(trame)) {
         strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(strcat(chaine, "Trame "), trame), " | Longitude : "), convertirLatitude(extraireChamps(trame,2), extraireChamps(trame,3)))," | Latitude : "),
                              convertirLongitude(extraireChamps(trame,4), extraireChamps(trame,5))), " | Heure : "),convertirHeure(extraireChamps(trame,1))),"\n");
    } else {
        strcat(strcat(strcat(strcat(chaine, "Trame "),trame), " : format incorrect"),"\n");
        char* copie=NULL;
        copie=malloc(200*sizeof(char));
        strcpy(copie,chaine);
        return copie;
    }
    char* copie=NULL;
    copie=malloc(200*sizeof(char));
    strcpy(copie,chaine);
    return copie;
}
