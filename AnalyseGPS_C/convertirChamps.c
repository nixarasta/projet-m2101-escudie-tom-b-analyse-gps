/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 1                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Analyse de trame GPS                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Escudi�-Tom                                                  *
*                                                                             *
*  Nom-pr�nom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : convertirChamps.c                                         *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "verifierTrame.h"
#include "extraireChamps.h"
#include "convertirChamps.h"

char* convertirHeure(char * champHeure) {
    char chaine[20] = "";
    chaine[0] = champHeure[0];
    chaine[1] = champHeure[1];
    chaine[2] = 104;
    chaine[3] = champHeure[2];
    chaine[4] = champHeure[3];
    chaine[5] = 109;
    chaine[6] = champHeure[4];
    chaine[7] = champHeure[5];
    chaine[8] = 115;

    char* copie=NULL;
    copie=malloc(9*sizeof(char));
    strcpy(copie,chaine);
    return copie;
}



char* convertirLatitude(char* champLatitude, char* champLatitudeCardinal) {
    char chaine[20] = "";

    float secondes = ((strtol(extraireSecondesDArc(champLatitude), NULL, 10))/100.00)*60.00;
    char buf[5];
    gcvt(secondes, 5, buf);

    for (int i = 0 ; i <=12 ; i++) {
        chaine[i] = 48;
    }

    chaine[0] = champLatitude[0];
    chaine[1] = champLatitude[1];
    chaine[2] = 42;
    chaine[3] = champLatitude[2];
    chaine[4] = champLatitude[3];
    chaine[5] = 39;
    chaine[8] = 46;
    chaine[11] = 34;
    chaine[12] = champLatitudeCardinal[0];

    if (secondes < 10) {
        chaine[10] = buf[0];
    }
    if (secondes > 10) {
        chaine[9] = buf[0];
        chaine[10] = buf[1];
    }
    if (secondes > 100) {
        chaine[7] = buf[0];
        chaine[9] = buf[1];
        chaine[10] = buf[2];
    }
    if (secondes > 1000) {
        chaine[6] = buf[0];
        chaine[7] = buf[1];
        chaine[9] = buf[2];
        chaine[10] = buf[3];
    }

    char* copie=NULL;
    copie=malloc(13*sizeof(char));
    strcpy(copie,chaine);
    return copie;
}

char* convertirLongitude(char* champLongitude, char* champLongitudeCardinal)
{
    char chaine[20] = "";

    float secondes = ((strtol(extraireSecondesDArc(champLongitude), NULL, 10))/100.00)*60.00;
    char buf[5];
    gcvt(secondes, 5, buf);

    for (int i = 0 ; i <=12 ; i++) {
        chaine[i] = 48;
    }

    chaine[0] = champLongitude[0];
    chaine[1] = champLongitude[1];
    chaine[2] = champLongitude[2];
    chaine[3] = 42;
    chaine[4] = champLongitude[3];
    chaine[5] = champLongitude[4];
    chaine[6] = 39;
    chaine[9] = 46;
    chaine[12] = 34;
    chaine[13] = champLongitudeCardinal[0];

    if (secondes < 10) {
        chaine[11] = buf[0];
    }
    if (secondes > 10) {
        chaine[10] = buf[0];
        chaine[11] = buf[1];
    }
    if (secondes > 100) {
        chaine[8] = buf[0];
        chaine[10] = buf[1];
        chaine[11] = buf[2];
    }
    if (secondes > 1000) {
        chaine[7] = buf[0];
        chaine[8] = buf[1];
        chaine[10] = buf[2];
        chaine[11] = buf[3];
    }

    char* copie=NULL;
    copie=malloc(13*sizeof(char));
    strcpy(copie,chaine);
    return copie;
}
